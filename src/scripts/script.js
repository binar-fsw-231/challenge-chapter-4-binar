class Player {
  constructor() {
    this.batu = document.getElementsByClassName("batu");
    this.kertas = document.getElementsByClassName("kertas");
    this.gunting = document.getElementsByClassName("gunting");
  }
}

const Computer = (Base) =>
  class extends Base {
    randomPick = (max) => Math.floor(Math.random() * max);
  };

class Player_1 extends Player {
  constructor(batu, kertas, gunting) {
    super(batu, kertas, gunting);
    this.#initiation();
  }

  #initiation() {
    this.batu[0].id = "batu-player";
    this.kertas[0].id = "kertas-player";
    this.gunting[0].id = "gunting-player";
  }
}

class Player_2 extends Computer(Player) {
  constructor(batu, kertas, gunting) {
    super(batu, kertas, gunting);
    this.#initiation();
  }

  #initiation() {
    this.batu[1].id = "batu-com";
    this.kertas[1].id = "kertas-com";
    this.gunting[1].id = "gunting-com";
  }
}

class Rules {
  constructor() {
    this.resultText = document.createElement("H1");
    this.resultContainer = document.getElementById("vs_result");
    this.user_choice;
    this.com_choice;
  }

  logger = (text) => {
    console.log(text);
  };

  defaultEvent = () => {
    this.resultContainer.classList.remove("draw");
    this.resultContainer.classList.remove("versus_result");
    this.resultText.innerHTML = "VS";
    this.resultContainer.appendChild(this.resultText);
  };

  winResult = () => {
    this.resultContainer.classList.remove("draw");
    this.resultContainer.classList.add("versus_result");
    this.resultText.innerHTML = "PLAYER WIN";
    this.resultContainer.appendChild(this.resultText);
  };

  loseResult = () => {
    this.resultContainer.classList.remove("draw");
    this.resultContainer.classList.add("versus_result");
    this.resultText.innerHTML = "COM WIN";
    this.resultContainer.appendChild(this.resultText);
  };

  drawResult = () => {
    this.resultContainer.classList.add("versus_result");
    this.resultContainer.classList.add("draw");
    this.resultText.innerHTML = "DRAW";
    this.resultContainer.appendChild(this.resultText);
  };

  decision = (userChoice, computerChoice) => {
    if ((userChoice === "batu" && computerChoice === "batu") || (userChoice === "kertas" && computerChoice === "kertas") || (userChoice === "gunting" && computerChoice === "gunting")) {
      return this.drawResult();
    } else if ((userChoice === "batu" && computerChoice === "gunting") || (userChoice === "kertas" && computerChoice === "batu") || (userChoice === "gunting" && computerChoice === "kertas")) {
      return this.winResult();
    } else if ((userChoice === "batu" && computerChoice === "kertas") || (userChoice === "kertas" && computerChoice === "gunting") || (userChoice === "gunting" && computerChoice === "batu")) {
      return this.loseResult();
    }
  };
}

class Game extends Rules {
  constructor(user_choice, com_choice) {
    super(user_choice, com_choice);
    this.resetResult = document.getElementById("reset");
    this.#initiation();
  }

  #initiation() {
    this.user = new Player_1();
    this.com = new Player_2();
    this.defaultEvent();
    this.resetButton();
  }

  getUserPick = (choice) => {
    this.user_choice = choice;
    this.logger(`Player choose: ${this.user_choice}`);
    return this.user_choice;
  };

  getComPick = (choice) => {
    this.com_choice = choice;
    this.logger(`Com choose: ${this.com_choice}`);
    return this.com_choice;
  };

  setPlayerListener = () => {
    this.user.batu[0].onclick = () => {
      this.getUserPick("batu");
      this.user.batu[0].classList.add("active_choice");
      this.user.kertas[0].classList.remove("active_choice");
      this.user.gunting[0].classList.remove("active_choice");
      this.removePlayerListener();
      this.decideResult();
    };

    this.user.kertas[0].onclick = () => {
      this.getUserPick("kertas");
      this.user.batu[0].classList.remove("active_choice");
      this.user.kertas[0].classList.add("active_choice");
      this.user.gunting[0].classList.remove("active_choice");
      this.removePlayerListener();
      this.decideResult();
    };

    this.user.gunting[0].onclick = () => {
      this.getUserPick("gunting");
      this.user.batu[0].classList.remove("active_choice");
      this.user.kertas[0].classList.remove("active_choice");
      this.user.gunting[0].classList.add("active_choice");
      this.removePlayerListener();
      this.decideResult();
    };
  };

  setComListener(choice) {
    switch (choice) {
      case "batu":
        this.getComPick("batu");
        this.com.batu[1].classList.add("active_choice");
        this.com.kertas[1].classList.remove("active_choice");
        this.com.gunting[1].classList.remove("active_choice");
        break;
      case "kertas":
        this.getComPick("kertas");
        this.com.batu[1].classList.remove("active_choice");
        this.com.kertas[1].classList.add("active_choice");
        this.com.gunting[1].classList.remove("active_choice");
        break;
      case "gunting":
        this.getComPick("gunting");
        this.com.batu[1].classList.remove("active_choice");
        this.com.kertas[1].classList.remove("active_choice");
        this.com.gunting[1].classList.add("active_choice");
        break;
      default:
        break;
    }
  }

  removePlayerListener = () => {
    document.getElementsByClassName("batu")[0].disabled = true;
    document.getElementsByClassName("kertas")[0].disabled = true;
    document.getElementsByClassName("gunting")[0].disabled = true;
  };

  result = () => {
    setInterval(() => {
      if (this.user_choice && this.com_choice) {
        this.decision(this.user_choice, this.com_choice);
      }
      this.user_choice = null;
      this.com_choice = null;
    }, 400);
  };

  decideResult() {
    switch (this.com.randomPick(3)) {
      case 2:
        this.setComListener("batu");
        this.result();
        break;
      case 1:
        this.setComListener("kertas");
        this.result();
        break;
      case 0:
        this.setComListener("gunting");
        this.result();
        break;
      default:
        break;
    }
  }

  resetButton() {
    this.resetResult.onclick = () => {
      this.logger("Game reset");
      this.defaultEvent();
      document.querySelectorAll(".choice").forEach((userButton) => {
        userButton.classList.remove("active_choice");
        userButton.disabled = false;
      });
    };
  }

  play() {
    this.setPlayerListener();
  }
}

const game = new Game();
game.play();
